import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class DenetService {
    denetRes: {body: string, id: number, title: string, userId: number};


    constructor(private http: HttpClient) { }

      getDenateValues(){
        let promise = new Promise((resolve, reject) => {
            let apiURL = `https://jsonplaceholder.typicode.com/posts/1`;
            this.http.get(apiURL)
            .toPromise()
            .then((res: any) => {
                    this.denetRes = res;
                    resolve(res);
                },(error) => {
                    reject(error);
                }
            );
        });
        return promise;
    }

    postDenateValues(coords: {value1: boolean, value2: boolean}){
        console.log(coords);
    //     let promise = new Promise((resolve, reject) => {
    //         let apiURL = `https://jsonplaceholder.typicode.com/posts/1`;
    //         this.http.post(apiURL, JSON.stringify({coords}))
    //         .toPromise()
    //         .then((res: any) => {
    //                 resolve(res);
    //             },(error) => {
    //                 reject(error);
    //             }
    //         );
    //     });
    //     return promise;
    }
}