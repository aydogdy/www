import { Component } from '@angular/core';
import { DenetService } from '../services/denet-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  isWorkinghours: boolean = true;
  isWorkingEnabled: boolean = false;
  denetRes: {body: string, id: number, title: string, userId: number};

  constructor(private denetService: DenetService){
    this.denetService.getDenateValues().then((res: any)=>{
      console.log(res);
      this.denetRes = res;
    }, (error)=>{
      console.log(error);
    });
  }

  onChangeWorkHours(type: number){
    switch (type) {
      case 0:
          this.isWorkingEnabled = !this.isWorkingEnabled;
          console.log("isWorkingEnabled: " + this.isWorkingEnabled);
          this.postDenateValues();
        break;
      case 1:
        this.isWorkinghours = !this.isWorkinghours;
        console.log("isWorkinghours: " + this.isWorkinghours);
        break;
      default:
        break;
    } 
  }

  postDenateValues(){
    let coords = {
      'value1': this.isWorkingEnabled,
      'value2': this.isWorkinghours
    };
    this.denetService.postDenateValues(coords);
  }


  public lineChartData:Array<any> = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'}
  ];
  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
 
  public randomize():void {
    let _lineChartData:Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }


}
